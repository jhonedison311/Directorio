
package directorio;


import java.util.Hashtable;
import java.util.Enumeration;
public class Directorio //Definicion variables
{
   private Hashtable <String, String> contactos;
   //Definicion de metodos
   
   public Directorio()
   {
       this.contactos = new Hashtable <String, String>();       
   }
   public int AgregarContacto(String Apodo,String Telefono)     //retorna 0 si no se pudo agragar y 1 si la pudo agregar
   {
       int resultado=0;                         //Definimos el resultado de la variable
       if(!this.contactos.containsKey(Apodo))   //Validamos que el contacto no esta ya registrado
       {
           this.contactos.put(Apodo,Telefono);  //Agregamos el contacto
           resultado=1;                         //Actualizamoos el valor de la variable resultado
       }
       return resultado;                      //Retornamos el resultado de la operacion
                         
   }
   public int ModificarContacto(String Apodo)
   {
       String resultado="";
       
       return resultado;
   }
   public int BorrarContacto()
   {
       int resultado=0;
       //Tarea definir metodo
       return resultado;
   }
    
   public void BorrarTodo ()
   {
       //Definir metodo
   }
   public String BuscarContacto(String Apodo)
   {
       String resultado="";
       
       return resultado;
   }
   public void ListarContactos()
   {
       Enumeration Lista = this.contactos.keys();
       Object Apodo;    
       Object Telefono;
       while(Lista.hasMoreElements())
       {
            Apodo=Lista.nextElement();
            Telefono=this.contactos.get(Apodo);
            System.out.println(Apodo+"--"+Telefono);
       }
   }
   
           
}
